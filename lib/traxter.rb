require "traxter/version"

module Traxter

  class Log

    INFO = 0
    ERROR = 1
    WARNING = 2
    DEBUG = 3
    TRACE = 4

    # private_class_method :new
    @@inst = nil
    @@fd = nil
    @name = ""
    @level = -1

    # def Traxter.instance
    #   @@inst = new unless @@inst
    #   @@inst
    # end

    def self.log_levels
      { :INFO => INFO, :ERROR => ERROR, :WARNING => WARNING, :DEBUG => DEBUG, :TRACE => TRACE }
    end

    def self.to_s
      s = ""
      s << "Logging to file " << @name << "\n" << "Level = "
      s << "INFO" if @level == INFO
      s << "ERROR" if @level == ERROR
      s << "WARNING" if @level == WARNING
      s << "DEBUG" if @level == DEBUG
      s << "TRACE" if @level == TRACE
      s << "Unknown" if @level < 0
      s << "\nMode = "
      s << "Append" if @mode == 'a'
      s << "Overwrite" if @mode == 'w'
      s << "Unknown" if @mode != 'a' && @mode != 'w'
      return s
    end

    def self.open(name, lvl, m = 'a', extendWithPid = false)
      @level = lvl
      @name = name
      @mode = m

      unless Dir.exists?(File.dirname(name))
        FileUtils.mkdir_p(File.dirname(name))
      end

      @name += '.' + $$.to_s if extendWithPid == true
      @@fd = File.new(@name, @mode) if @@fd == nil
      if (!@@fd) then
        puts ("Cannot open file " + @name)
        return false
      end
      true
    end

    def self.open?
      @@fd.present?
    end

    def self.name
      @name
    end

    def self.level
      @level
    end

    def self.close()
      @@fd.close if @@fd != 0
    end

    def self.write (msg)
      s = ""
      s << pfx() << ' '<< msg << "\n"
      if @@fd.nil?
        puts s
      else
        @@fd.write(s)
        @@fd.flush
      end
    end

    def self.pfx()
      return Time::now().strftime("%d%b%y %H:%M:%S")
    end

    def self.info(msg)
      write ('[INFO] ' + msg);
    end

    # A replica of logInfo as rspec does funny things with the log_info method
    def self.information(msg)
      write ('[INFO] ' + msg);
    end

    def self.trace(msg)
      write ('[TRACE] ' + msg) if @level >= TRACE
    end

    def self.debug(msg)
      write ('[DEBUG] ' + msg) if @level >= DEBUG
    end

    def self.warning(msg)
      write ('[WARNING] ' + msg) if @level >= WARNING
    end

    def self.error(msg)
      write ('[ERROR] ' + msg) if @level >= ERROR
    end

  end
end
